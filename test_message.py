from utils import is_allowed_message, is_valid_size


def test_message_ok():
    assert is_allowed_message("test") is True
    assert is_allowed_message("тест") is True
    assert is_allowed_message("test test test") is True
    assert is_allowed_message("test test 123123125321") is True
    assert is_allowed_message("тест, тест") is True
    assert is_allowed_message("тест!") is True
    assert is_allowed_message("тест?") is True
    assert is_allowed_message("тест.") is True
    assert is_allowed_message("ё") is True
    assert is_allowed_message("тест - тест") is True
    assert is_allowed_message("тест _ тест") is True
    assert is_allowed_message(":somesmile:") is True
    assert is_allowed_message("%") is True
    assert is_allowed_message("%some text%") is True
    assert is_allowed_message("тест. тест! тест? тест-тест? test, test. http://test.com/test") is True


def test_message_bad():
    assert is_allowed_message("@all") is False
    assert is_allowed_message("@here") is False
    assert is_allowed_message("@ here") is False
    assert is_allowed_message("qqqq @here") is False
    assert is_allowed_message(r"\@here") is False
    assert is_allowed_message("()[]") is False
    assert is_allowed_message("()") is False
    assert is_allowed_message("[]") is False
    assert is_allowed_message("汉字") is False


def test_message_len_ok():
    assert is_valid_size("ф") is True
    assert is_valid_size("a" * 899) is True


def test_message_len_bad():
    assert is_valid_size("") is False
    assert is_valid_size("a" * 900) is False
    assert is_valid_size("a" * 1000) is False
