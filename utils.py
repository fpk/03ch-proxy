import re

RE_VALID_MESSAGE = re.compile(r"^([%_ё!?а-яА-Яa-zA-Z0-9\.,:\-\\/ ]+)$")


def is_allowed_message(msg: str) -> bool:
    return RE_VALID_MESSAGE.match(msg) is not None


def is_valid_size(msg: str) -> bool:
    size = len(msg)
    return 0 < size < 900
